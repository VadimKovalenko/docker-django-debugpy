
# Running and Debugging Django Application with Docker Compose

This guide outlines how to run a Django application using Docker Compose and how to debug it using debugpy.

## Prerequisites
- Docker and Docker Compose installed
- Django application ready for deployment
- `requirements.txt` file with necessary dependencies, including `debugpy` and `django-debug-toolbar`

## Files Overview

1. `docker-compose.yml`: Defines the services, networks, and volumes for your application.
2. `Dockerfile`: Instructions for building your Docker image.
3. `requirements.txt`: Lists dependencies for your Django project.
4. `launch.json`: Configuration file for debugging in Visual Studio Code.

## Setup

### Docker Compose File (`docker-compose.yml`)

```yaml
version: '3.8'
services:
  web:
    build: .
    entrypoint: ["/usr/local/bin/python"]
    command: 
      - "-m"
      - "debugpy"
      - "--wait-for-client"
      - "--listen"
      - "0.0.0.0:5678"
      - "manage.py"
      - "runserver"
      - "0.0.0.0:8000"
    ports:
      - "8000:8000"
      - "5678:5678"
    volumes:
      - .:/app
```

### Dockerfile

```Dockerfile
FROM quay.io/azavea/django:3.2-python3.7-slim
ENV PYTHONUNBUFFERED 1
WORKDIR /app
COPY requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt
COPY . /app/
EXPOSE 8000 5678
CMD ["python", "-m", "debugpy", "--wait-for-client", "--listen", "0.0.0.0:5678", "manage.py", "runserver", "0.0.0.0:8000"]
```

### Requirements File (`requirements.txt`)

```
debugpy>=1.0.0
django-debug-toolbar
```

## Running the Application

Execute the following command to build and run your Django application:

```bash
docker-compose up --build
```

The application should be accessible at `http://localhost:8000`.

## Debugging Setup

### Visual Studio Code Configuration (`launch.json`)

```json
{
    "configurations": [
        {
          "name": "Azavea OSHub Django: Remote Attach",
          "type": "python",
          "request": "attach",
          "connect": {
            "host": "localhost",
            "port": 5678
          },
          "pathMappings": [
            {
              "localRoot": "${workspaceFolder}",
              "remoteRoot": "/app"
            }
          ]
        }
    ]
}
```

### Debugging Steps

1. Start your Django application with `docker-compose up`.
2. Open Visual Studio Code.
3. Go to the Run and Debug view (Ctrl+Shift+D).
4. Select the "Azavea OSHub Django: Remote Attach" configuration.
5. Set breakpoints and start the debugger.
6. Go to http://localhost:8000/playground/hello/, breakpoints should work now.

Now, you can set breakpoints and debug your Django application running inside a Docker container.
