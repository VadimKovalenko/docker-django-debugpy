# Use the official Python 3.7 slim image as a base image
# FROM python:3.7-slim

# TODO connect openssupplyhub image and test

# Base image that used in opensupply hub, probably wont be able to debug
# And you got this error while building: 
# gunicorn: error: argument -m/--umask: invalid auto_int value: 'debugpy'
# Maybe you can override CMD inside this image
FROM quay.io/azavea/django:3.2-python3.7-slim

# Set environment variables
ENV PYTHONUNBUFFERED 1

# Create and set the working directory in the container
WORKDIR /app

# Copy the requirements file into the container
COPY requirements.txt /app/

# Install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the current directory contents into the container at /app
COPY . /app/

# Expose ports for web server and debug server
EXPOSE 8000 5678

# Command to start the Django development server with debugpy
CMD ["python", "-m", "debugpy", "--wait-for-client", "--listen", "0.0.0.0:5678", "manage.py", "runserver", "0.0.0.0:8000"]
